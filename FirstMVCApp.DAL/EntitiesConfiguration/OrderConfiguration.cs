﻿using FirstMVCApp.DAL.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FirstMVCApp.DAL.EntitiesConfiguration
{
    public class OrderConfiguration : BaseEntityConfiguration<Order>
    {
        protected override void ConfigureProperties(EntityTypeBuilder<Order> builder)
        {
            builder.Property(p => p.Amount)
                .IsRequired();

            builder.Property(p => p.CreatedOn)
                .IsRequired();

            builder.Property(p => p.Sum)
                .IsRequired();

            builder.HasIndex(b => b.Sum).IsUnique(false);
        }
        
        protected override void ConfigureForeignKeys(EntityTypeBuilder<Order> builder)
        {
            builder
                .HasOne(b => b.Product)
                .WithMany(b => b.Orders)
                .HasForeignKey(b => b.ProductId)
                .IsRequired();
        }
    }
}