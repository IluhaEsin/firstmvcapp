﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FirstMVCApp.DAL.Migrations
{
    public partial class SetDataToCustomerId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                    Update Orders
                    set CustomerId = (Select Id from Customers where Name = Orders.CustomerName)
                    ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                Update Orders
                set CustomerId = NULL
                ");
        }
    }
}
