﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FirstMVCApp.DAL.Migrations
{
    public partial class AddDataToCustomerTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                        Insert into Customers (Name)
                        select CustomerName from Orders
                        ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                        delete from Customers
                        ");
        }
    }
}
