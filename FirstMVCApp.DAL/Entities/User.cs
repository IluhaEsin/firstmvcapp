﻿using Microsoft.AspNetCore.Identity;

namespace FirstMVCApp.DAL.Entities
{
    public class User : IdentityUser<int>
    {
        public string Bucket { get; set; }
    }
}
