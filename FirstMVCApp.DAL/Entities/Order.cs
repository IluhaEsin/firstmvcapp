﻿using System;

namespace FirstMVCApp.DAL.Entities
{
    public class Order : Entity
    {
        public int Amount { get; set; }
        public string Description { get; set; }
        public string CustomerName { get; set; }
        public DateTime CreatedOn { get; set; }
        public decimal Sum { get; set; }
        public int ProductId { get; set; }
        public Product Product { get; set; }
    }
}
