﻿using FirstMVCApp.DAL.Entities;

namespace FirstMVCApp.DAL.Repositories.Contracts
{
    public interface ICategoryRepository : IRepository<Category>
    {
    }
}
