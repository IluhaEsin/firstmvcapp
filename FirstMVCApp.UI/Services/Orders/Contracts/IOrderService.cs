﻿using System.Collections.Generic;
using FirstMVCApp.DAL.Entities;
using FirstMVCApp.UI.Models.Orders;

namespace FirstMVCApp.UI.Services.Orders.Contracts
{
    public interface IOrderService
    {
        OrderCreateModel GetOrderCreateModel(int productId);
        Order CreateOrder(OrderCreateModel model);
        List<OrderIndexModel> GetOrdersList();
    }
}
