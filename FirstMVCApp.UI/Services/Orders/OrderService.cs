﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using FirstMVCApp.DAL;
using FirstMVCApp.DAL.Entities;
using FirstMVCApp.UI.Models.Orders;
using FirstMVCApp.UI.Models.Products;
using FirstMVCApp.UI.Services.Orders.Contracts;

namespace FirstMVCApp.UI.Services.Orders
{
    public class OrderService : IOrderService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public OrderService(IUnitOfWorkFactory unitOfWorkFactory)
        {
            if (unitOfWorkFactory == null)
                throw new ArgumentNullException(nameof(unitOfWorkFactory));

            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public OrderCreateModel GetOrderCreateModel(int productId)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var product = unitOfWork.Products.GetByIdWithBrandAndCategory(productId);

                var orderCreateModel = new OrderCreateModel()
                {
                    ProductId = productId,
                    Product = Mapper.Map<ProductModel>(product)
                };

                return orderCreateModel;
            }
        }

        public Order CreateOrder(OrderCreateModel model)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var order = Mapper.Map<Order>(model);
                order.CreatedOn = DateTime.Now;
                var product = unitOfWork.Products.GetById(order.ProductId);
                order.Sum = order.Amount * product.Price;

                order = unitOfWork.Orders.Create(order);

                return order;
            }
        }

        public List<OrderIndexModel> GetOrdersList()
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var orders = unitOfWork.Orders.GetAllWithProduct().ToList();

                return Mapper.Map<List<OrderIndexModel>>(orders);
            }
        }
    }
}
