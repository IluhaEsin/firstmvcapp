﻿using System.Collections.Generic;
using FirstMVCApp.UI.Models.Products;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace FirstMVCApp.UI.Services.Products.Contracts
{
    public interface IProductService
    {
        List<ProductModel> SearchProducts(ProductFilterModel model);
        ProductCreateModel GetProductCreateModel();
        void CreateProduct(ProductCreateModel model);
        SelectList GetCategoriesSelect();
        SelectList GetBrandsSelect();
        ProductEditModel GetProductById(int id);
        void EditProduct(ProductEditModel model);
    }
}