﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using FirstMVCApp.DAL;
using FirstMVCApp.DAL.Entities;
using FirstMVCApp.UI.Models.Products;
using FirstMVCApp.UI.Services.Products.Contracts;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace FirstMVCApp.UI.Services.Products
{
    public class ProductService : IProductService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public ProductService(IUnitOfWorkFactory unitOfWorkFactory)
        {
            if (unitOfWorkFactory == null)
                throw new ArgumentNullException(nameof(unitOfWorkFactory));

            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public List<ProductModel> SearchProducts(ProductFilterModel model)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                IEnumerable<Product> products = unitOfWork.Products.GetAllWithCategoriesAndBrands();
                
                products = products
                    .ByPriceFrom(model.PriceFrom)
                    .ByPriceTo(model.PriceTo)
                    .ByName(model.Name)
                    .ByCategoryId(unitOfWork, model.CategoryId)
                    .ByBrandId(unitOfWork, model.BrandId);

                List<ProductModel> productModels = Mapper.Map<List<ProductModel>>(products);
                
                return productModels;
            }
        }

        public ProductCreateModel GetProductCreateModel()
        {
            return new ProductCreateModel()
            {
                CategoriesSelect = GetCategoriesSelect(),
                BrandsSelect = GetBrandsSelect()
            };
        }

        public void CreateProduct(ProductCreateModel model)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var product = Mapper.Map<Product>(model);

                unitOfWork.Products.Create(product);
            }
        }

        public SelectList GetCategoriesSelect()
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var categories = unitOfWork.Categories.GetAll().ToList();
                return new SelectList(categories, nameof(Category.Id), nameof(Category.Name));
            }
        }

        public SelectList GetBrandsSelect()
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var brands = unitOfWork.Brands.GetAll().ToList();
                return new SelectList(brands, nameof(Category.Id), nameof(Category.Name));
            }
        }

        public ProductEditModel GetProductById(int id)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var product = unitOfWork.Products.GetById(id);

                var model = Mapper.Map<ProductEditModel>(product);

                var categories = unitOfWork.Categories.GetAll().ToList();
                var brands = unitOfWork.Brands.GetAll().ToList();
                
                model.CategoriesSelect = new SelectList(
                    categories, 
                    nameof(Category.Id),
                    nameof(Category.Name),
                    model.CategoryId);

                model.BrandsSelect = new SelectList(
                    brands,
                    nameof(Brand.Id),
                    nameof(Brand.Name),
                    model.BrandId);

                return model;
            }
        }

        public void EditProduct(ProductEditModel model)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var product = Mapper.Map<Product>(model);
                unitOfWork.Products.Update(product);
            }
        }
    }
}
