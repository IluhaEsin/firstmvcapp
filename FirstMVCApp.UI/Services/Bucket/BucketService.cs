﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FirstMVCApp.DAL;
using FirstMVCApp.DAL.Entities;
using FirstMVCApp.UI.Models.Bucket;
using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;

namespace FirstMVCApp.UI.Services.Bucket
{
    public interface IBucketService
    {
        Task AddToBucketAsync(int productId, User user);
    }

    public class BucketService : IBucketService
    {
        public readonly UserManager<User> _userManager;
        public readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public BucketService(UserManager<User> userManager,
            IUnitOfWorkFactory unitOfWorkFactory)
        {
            _userManager = userManager;
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public async Task AddToBucketAsync(int productId, User user)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var product = unitOfWork.Products.GetById(productId);
                if (product == null)
                    throw new ArgumentOutOfRangeException(nameof(productId), $"No product with id {productId}");

                var bucketItems = string.IsNullOrWhiteSpace(user.Bucket) ? 
                    CreateBucket(productId) :
                    UpdateBucket(productId, user.Bucket);

                user.Bucket = JsonConvert.SerializeObject(bucketItems);
                await _userManager.UpdateAsync(user);
            }
        }

        private List<BucketItem> UpdateBucket(int productId, string currentBucket)
        {
            try
            {
                var currentItems = JsonConvert.DeserializeObject<List<BucketItem>>(currentBucket);
                var existingItem = currentItems.FirstOrDefault(c => c.ProductId == productId);
                if (existingItem != null)
                {
                    existingItem.Qty++;
                }
                else
                {
                    currentItems.Add(
                        new BucketItem()
                        {
                            ProductId = productId,
                            Qty = 1
                        }
                    );
                }

                return currentItems;
            }
            catch
            {
                return CreateBucket(productId);
            }
        }

        private List<BucketItem> CreateBucket(int productId)
        {
            return new List<BucketItem>()
            {
                new BucketItem()
                {
                    ProductId = productId,
                    Qty = 1
                }
            };
        }
    }
}
