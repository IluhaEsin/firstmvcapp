﻿using AutoMapper;
using FirstMVCApp.DAL.Entities;
using FirstMVCApp.UI.Models.Orders;
using FirstMVCApp.UI.Models.Products;

namespace FirstMVCApp
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateProductToProductModelMap();
            CreateProductCreateModelToProduct();
            CreateOrderCreateModelToOrderMap();
            CreateOrderToOrderIndexModelMap();
            CreateUniversalMap<Product, ProductEditModel>();
        }

        private void CreateProductToProductModelMap()
        {
            CreateMap<Product, ProductModel>()
                .ForMember(target => target.BrandName,
                    src => src.MapFrom(p => p.BrandId == null ? ProductModel.NoBrand : p.Brand.Name))
                .ForMember(target => target.CategoryName,
                    src => src.MapFrom(p => p.Category.Name));
        }

        private void CreateProductCreateModelToProduct()
        {
            CreateMap<ProductCreateModel, Product>();
        }

        private void CreateOrderCreateModelToOrderMap()
        {
            CreateMap<OrderCreateModel, Order>();
        }

        private void CreateOrderToOrderIndexModelMap()
        {
            CreateMap<Order, OrderIndexModel>()
                .ForMember(target => target.Price,
                src => src.MapFrom(p => p.Product.Price))
                .ForMember(target => target.ProductName,
                    src => src.MapFrom(p => $"{p.Product.Brand.Name} {p.Product.Name}"))
                .ForMember(target => target.OrderCreatedOn,
                    src => src.MapFrom(p => p.CreatedOn))
                .ForMember(target => target.OrderSum,
                    src => src.MapFrom(p => p.Sum));
        }

        private void CreateUniversalMap<From, To>()
        {
            CreateMap<From, To>();
            CreateMap<To, From>();
        }
    }
}
