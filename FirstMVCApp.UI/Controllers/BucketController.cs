﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using FirstMVCApp.DAL.Entities;
using FirstMVCApp.UI.Services.Bucket;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace FirstMVCApp.UI.Controllers
{
    public class BucketController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly IBucketService _bucketService;

        public BucketController(UserManager<User> userManager, IBucketService bucketService)
        {
            _userManager = userManager;
            _bucketService = bucketService;
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> Add(int? productId)
        {
            if (!productId.HasValue)
            {
                ViewBag.BadRequestMessage = "Product Id can not be NULL";
                return View("BadRequest");
            }

            try
            {
                User currentUser = await _userManager.GetUserAsync(User);
                await _bucketService.AddToBucketAsync(productId.Value, currentUser);
                return RedirectToAction("Index", "Product");
            }
            catch (Exception e)
            {
                return StatusCode(500, e);
            }
        }
    }
}
