﻿using System;

namespace FirstMVCApp.UI.Models.Orders
{
    public class OrderIndexModel
    {
        public string ProductName { get; set; }
        public decimal Price { get; set; }
        public DateTime OrderCreatedOn { get; set; }
        public string OrderSum { get; set; }
    }
}
