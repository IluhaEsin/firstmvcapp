﻿using System.ComponentModel.DataAnnotations;
using FirstMVCApp.UI.Models.Products;

namespace FirstMVCApp.UI.Models.Orders
{
    public class OrderCreateModel
    {
        public ProductModel Product { get; set; }

        [Required]
        public int ProductId { get; set; }

        [Required]
        [Display(Name = "Количество")]
        public int Amount { get; set; }

        [Display(Name = "Комментарий")]
        public string Description { get; set; }
    }
}
